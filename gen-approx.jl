push!(LOAD_PATH, ".")
push!(LOAD_PATH, "..")

using Playground
using Glob
import JSON
# elist = Experiments.prepare()
# X = pmap(Experiments.dispatch, elist)

L = [:(load_vectors("dbs/nasa.vecs")),
     :(load_vectors("dbs/colors.vecs")),
     :(load_vectors("dbs/bigann_base-1M.vecs", "dbs/texmex/ascii/bigann_query-500.vecs", ntype=Float32)),
     :(load_glove("dbs/glove.twitter.27B.100d.txt")),
     # :(load_glove("dbs/glove/glove.6B.100d.txt")),
     ]

# for dbpath in vcat(glob("dbs/random/clusters-n=1000000-d=*-c=*.vecs"),
#                   glob("dbs/random/uniform-n=1000000-d=*.vecs"))
# for dbpath in glob("dbs/random/uniform-n=1000000-d=*.vecs")
#     push!(L, :(load_vectors($dbpath)))
# end

I = Expr[]

for benchmark in L
    push!(I, :(run_seq($benchmark)))
    push!(I, :(run_knr($benchmark, 5, 0, 0.9)))
    for stype in [:BeamSearch, :IteratedHillClimbingSearch, :Is2014Search, :SteadyStateSearch]
        for ntype in [:LogarithmicNeighborhood, :GallopingNeighborhood, :EssencialNeighborhood]
            for recall in [0.6, 0.9, 0.99]
                t = :(run_localsearch($benchmark, $recall, $stype(), $ntype()))
                push!(I, t)
            end
        end
    end
end


# E = [(db, index) for db=L, index=indexlist]

# #open("instructions.txt", "w") do f
for e in I
    # write(f, string(e), "\n")
    println(string(e))
end
# #end

# pmap(dispatch, E)
# generate_tables(OUTDIR)
