push!(LOAD_PATH, ".")

using NNS
using Playground
using Glob
import JSON

E = []
if length(ARGS) == 0
    for line in eachline(STDIN)
        instruction = parse(strip(line))
        push!(E, instruction)
    end
else
    for line in ARGS
        instruction = parse(strip(line))
        push!(E, instruction)
    end
end


if nprocs() == 1
    map(eval, E)
else
    pmap(eval, E)
end

generate_tables(OUTDIR)
