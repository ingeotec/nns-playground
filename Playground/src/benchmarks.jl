export generate_uniform, generate_random_clusters, load_vectors, load_glove_internal, load_glove


function generate_uniform(n, dim, numqueries=256)
    dbpath = "dbs/random/uniform-n=$(n)-d=$dim.vecs"
    qpath = string(dbpath, ".queries")
    if isfile(dbpath) && isfile(qpath)
        return dbpath, qpath
    end

    mkpath(dirname(dbpath))
    info("Generating $(dbpath)")
    db = Vector{Float32}[rand(dim) for i=1:n]
    saveDB(db, dbpath)
    db = Vector{Float32}[rand(dim) for i=1:numqueries]
    saveDB(db, qpath)
    return dbpath, qpath
end

function generate_random_clusters(n, dim; numclusters=0, numqueries=256)
    numclusters = numclusters == 0 ? Int(ceil(log2(n))) : numclusters
    dbpath = "dbs/random/clusters-n=$n-d=$dim-c=$numclusters.vecs"
    qpath = string(dbpath, ".queries")
    if isfile(dbpath) && isfile(qpath)
        return dbpath, qpath
    end

    mkpath(dirname(dbpath))
    info("Generating $(dbpath)")
    db = Array(Vector{Float32}, 0)
    for i=1:numclusters
        center = rand(dim) .* 100
        push!(db, center)
    end
    for i=numclusters+1:n
        center = db[rand(1:numclusters)]
        v = center + randn(dim)
        push!(db, v)
    end
    
    # shuffle!(db)
    queries = Array(Vector{Float32}, 0)
    for i=1:numqueries
        center = db[rand(1:numclusters)]
        v = center + randn(dim)
        push!(queries, v)
    end

    shuffle!(db)
    saveDB(db, dbpath)
    saveDB(queries, qpath)
    return dbpath, qpath
end

const CACHE = Dict()

function load_vectors(dbpath, qpath=nothing; ntype=Float32, dtype=L2Distance, k=KSEARCH, numqueries=NUMQUERIES)
    benchmark = joinpath(OUTDIR, basename(dbpath))
    mkpath(benchmark)
    
    function load()
        local db::Vector{Vector{ntype}}
        if haskey(CACHE, dbpath)
            db = CACHE[dbpath]
        else
            db = loadDB(Vector{ntype}, dbpath)
            CACHE[dbpath] = db
        end

        qpath = qpath == nothing ? string(dbpath, ".queries") : qpath
        
        if ispath(qpath)
            if haskey(CACHE, qpath)
                queries = CACHE[qpath]
            else
                CACHE[qpath] = queries = loadDB(Vector{ntype}, qpath)
            end
        else
            queries = rand(db, numqueries)
        end
    
        distfun = dtype()
        perf = Performance(db, distfun, queries, k)
        seq = Sequential(db, distfun)
        return perf, seq
    end

    return benchmark, load
end

function load_glove(dbpath, k=KSEARCH, numqueries=NUMQUERIES)
    benchmark = joinpath(OUTDIR, basename(dbpath))
    benchmark = replace(benchmark, ".txt", "")
    mkpath(benchmark)

    function load()
        if haskey(CACHE, dbpath)
            wdb = loadGloveDB(dbpath)
        else
            CACHE[dbpath] = loadGloveDB(dbpath)
            wdb = CACHE[dbpath]
        end
    
        db::Vector{Vector{Float32}} = wdb[2]
        distfun = AngleDistance()
        perf = Performance(db, distfun, numqueries, k)
        seq = Sequential(db, distfun)
        return perf, seq
    end
    return benchmark, load
end
