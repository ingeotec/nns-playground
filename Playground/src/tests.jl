export load_index, run_index, run_seq, run_knr, run_localsearch

function load_index(create::Function, load::Function, indexname::AbstractString, dist::Distance)
    if ispath(indexname)
        info("loading ", indexname)
        meta = [Symbol(k) => v for (k, v) in JSON.parsefile("$(indexname).meta")]
        index = load()
    else
        info("creating ", indexname)
        meta = Dict()
        buildtime = time()
        builddist = dist.calls
        index = create()
        buildtime = time() - buildtime
        builddist = dist.calls - builddist
        save(index, indexname)

        open("$(indexname).meta", "w") do f
            meta[:buildtime] = buildtime
            meta[:builddist] = builddist
            meta[:memory] = sum([stat(fname).size for fname in glob("$(indexname)*")])
            write(f, JSON.json(meta))
        end
    end
    return index, meta
end

function run_index(resname::AbstractString, perf::Performance, index::Index, meta_build::Dict)
    info("computing result ", resname)
    p = probe(perf, index)
    meta = Dict(
                :resname => resname,
                :length => length(index.db),
                :recall => p.recall,
                :seconds => p.seconds,
                :queriespersecond => 1 / p.seconds,
                :distances => p.distances,
                :type => split(string(typeof(index).name), '.')[end]
                )

    for name in fieldnames(index)
        value = getfield(index, name)
        if isa(value, Bool) || isa(value, Number) || isa(value, AbstractString)
            meta[name] = value
        end
    end

    meta[:buildtime] = meta_build[:buildtime]
    meta[:builddist] = meta_build[:builddist]
    meta[:memory] = meta_build[:memory]
    open(resname, "w") do f
        write(f, JSON.json(meta), "\n")
    end

    return meta, perf, index
end

function gettypename(T::Type)
    return replace(string(T), r"\{.+", "")
end

function load_meta(resname)
    info("loading $resname")
    [Symbol(k) => v for (k, v) in JSON.parsefile(resname)]
end

function run_seq(load_benchmark)
    benchmark, load = load_benchmark
    indexname = joinpath(benchmark, "SeqIndex")
    resname = "$(indexname).result"
    isfile(resname) && return load_meta(indexname)

    info("computing $resname")
    perf, seq = load()
    index, meta = load_index(() -> Sequential(seq.db, seq.dist),
                             () -> Sequential(indexname, seq.db, seq.dist),
                             indexname, seq.dist
                             )
    return run_index(resname, perf, index, meta)
end

function run_knr(load_benchmark, k=5, numrefs=0, recall=0.9)
    benchmark, load = load_benchmark
    perf, seq = load()
    db, dist = seq.db, seq.dist
    numrefs = numrefs == 0 ? Int(round(sqrt(length(db)))) : numrefs
    resultList = []
    indexname = joinpath(benchmark, "KnrIndex.numrefs=$(numrefs).k=$(k)")
    resname = "$(indexname).recall=$(recall).result"
    isfile(resname) && return load_meta(indexname)
    info("computing $resname")
    
    knr, m = load_index(() -> Knr(db, dist, numrefs, k),
                        () -> Knr(indexname, db, dist),
                        indexname, dist)

    optimize!(knr, recall, k)

    meta, perf, index = run_index(resname, perf, knr, m)
    meta[:type] = "$(meta[:type])-k=$(k)"
    push!(resultList, meta)

    return meta
end

#function run_localsearch(benchmark, perf, seq, searchAlgorithm, recall)
function run_localsearch(load_benchmark, recall, salgo, nalgo)
    benchmark, load = load_benchmark
    
    indexname = joinpath(benchmark, "LocalSearchIndex.search=$(string(salgo)).neighborhood=$(string(nalgo))")
    indexname = "$(indexname).recall=$(recall)"
    resname = "$(indexname).result"
    isfile(resname) && return load_meta(indexname)

    info("computing $resname")
    perf, seq = load()
    expected_k = perf.expected_k - perf.shift_expected_k
    
    idx, meta = load_index(() -> LocalSearchIndex(seq.db, seq.dist,
                                                  search_algo=salgo,
                                                  neighborhood_algo=nalgo,
                                                  expected_recall=recall,
                                                  expected_k=expected_k
                                                  ),
                           () -> LocalSearchIndex(indexname, seq.db, seq.dist),
                           indexname, seq.dist
                           )
    meta = run_index(resname, perf, idx, meta)
    return meta
end
