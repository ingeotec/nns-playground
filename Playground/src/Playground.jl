module Playground

using NNS
using JSON
using DataFrames
using Glob

# laesa = Laesa(db, distfun, 3)
# @show probe(perf, laesa)
# sss = Sss(db, distfun, 0.35)
# @show probe(perf, sss)
# kvp = Kvp(db, distfun, 3, 128)
# @show probe(perf, kvp)
# @time a = search(s, randn(8))
# ltournament = LaesaTournament(db, distfun, 4)
# @show probe(perf, ltournament)
#
# kvptournament = KvpTournament(db, distfun, 4, 30, 10)
# @show probe(perf, kvptournament)

const OUTDIR="output"
const KSEARCH=10
const NUMQUERIES=256

include("benchmarks.jl")
include("tests.jl")

export generate_tables, save_table, prepare, dispatch, OUTDIR

function generate_tables(outname)
    for dname in glob(joinpath(outname,"*/"))
        @show dname
        L = []
        for resname in glob("$(dname)/*.result")
            #metaname = replace(resname, r".result", ".meta")
            res = JSON.parsefile(resname)
            #for (k,v) in JSON.parsefile(metaname)
            #    res[k] = v
            #end
            push!(L, [Symbol(k) => v for (k, v) in res])
        end

        outname = string(rstrip(dname, '/'), ".tsv")
        save_table(outname, L)
    end
end

function save_table(outname, resultList)
    keySet = Set()
    for res in resultList
        for (k, v) in res
            union!(keySet, keys(res))
        end
    end

    # if there are several indexes we must normalize the set of keys for each entry
    dataframe = DataFrame()
    for key in keySet
        dataframe[key] = []
    end

    for r in resultList
        for k in keySet
            if !haskey(r, k)
                r[k] = NA
            end
        end
        push!(dataframe, r)
    end

    writetable(outname, dataframe, separator='\t')
end

# function plot_result_list()
#     using Gadfly
#     plot(
#         dataframe[ dataframe[:recall] .> 0.9, : ],
#         color="type", x="recall", y="queriespersecond",
#         Scale.y_log2, Geom.point, Geom.line,
#         Guide.xlabel("recall"),
#         Guide.ylabel("queries per second")
#     )
# end

# function dispatch(p)
#     @show "dispatching", p
#     (load_benchmark, r) = p
    
#     # run = eval(r[1])

#     if length(r) == 1
#         run(b, p, s)
#     else
#         run(b, p, s, r[2:end]...)
#     end
# end

end
