push!(LOAD_PATH, ".")

using NNS

global GLOVE = loadGloveDB("dbs/glove.6B.100d.txt")
global WORDS = GLOVE[1]
global VECS = GLOVE[2]

global VOC = [w => VECS[i] for (i, w) in enumerate(WORDS)]
global KNR = Knr("output/glove6B100d/KnrIndex.numrefs=632.k=5", VECS, AngleDistance())
optimize!(KNR, 0.95)

function wsearch(wlist, k=30)
    vector = zeros(Float32, 100)
    for w in wlist
        if w[1] == '+'
            vector += VOC[w[2:end]]
        elseif w[1] == '-'
            vector -= VOC[w[2:end]]
        else
            vector += VOC[w]
        end
    end

    [(WORDS[p.objID], p.dist) for p in search(KNR, vector, KnnResult(k))]
end

function wset(uword, vword, k=30)
    ures = wordsearch(uword, k)
    vres = wordsearch(vword, k)
    uset = Set([w for (w,d) in ures])
    vset = Set([w for (w,d) in vres])
    (uset, vset)
end

function wdist(word, wtarget, k=30)
    # [(distance(KNR.dist, VOC[word], VOC[w]), w) for p in search([wtarget])]
    L = []
    wvec = VOC[word]
    for p in search(KNR, VOC[wtarget], KnnResult(k))
        d = distance(KNR.dist, wvec, VECS[p.objID])
        row = (d, word, WORDS[p.objID])
        # info(row)
        push!(L, row)
    end
    return L
end

function xshow(a, b, k=15)
    info("======= checking $(a) -> $(b)")
    P = sort(wdist(a, b, k))
    Q = sort(wdist(b, a, k))
    for x in P
        info(x)
    end
    info("======= checking $(b) -> $(a)")
    for x in Q
        info(x)
    end
    info("@@@ info> $(a) -> $(b)")
    info([x[3] for x in P])
    info("@@@ info> $(b) -> $(a)")
    info([x[3] for x in Q])
end
