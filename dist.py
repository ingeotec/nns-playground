import os
import sys
import time
import subprocess


def run_one(cmdfile, line):
    cmd = ['julia', cmdfile, line]
    # print("RUNNING", cmd)
    return subprocess.Popen(cmd)


def wait_for_one(queue):
    qsize = len(queue)
    
    while True:
        _queue = []
        for proc in queue:
            if proc.poll() is None:
                _queue.append(proc)
        queue = _queue

        if len(queue) == qsize:
            time.sleep(0.01)
        else:
            return _queue


def main(cmdfile, inputfile, max_procs):
    # print("EXECUTING", inputfile)
    P = []
    while True:
        line = inputfile.readline()
        if len(line) == 0:
            break
        
        line = line.strip()
        if len(line) == 0 or line[0] == '#':
            continue

        if len(P) == max_procs:
            P = wait_for_one(P)

        P.append(run_one(cmdfile, line))

    while len(P) > 0:
        P = wait_for_one(P)


NUMPROCS = os.environ.get("NUMPROCS", max(1, os.cpu_count() // 4))
NUMPROCS = 12
cmdfile = sys.argv[1]

if len(sys.argv) == 2:
    main(cmdfile, sys.stdin, max_procs=NUMPROCS)
else:
    for inputfile in sys.argv[2:]:
        with open(inputfile) as f:
            main(cmdfile, f, max_procs=NUMPROCS)

