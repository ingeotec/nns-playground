#  Copyright 2016 Eric S. Tellez <eric.tellez@infotec.mx>
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

using NNS
export OBS, OBI, compute_aknn


type OBS # <: LocalSearchAlgorithm
end

### local search algorithm

function first_beam_local{T}(index::LocalSearchIndex{T}, qID::Int32, reslist::Vector{KnnResult}, tabu::Set{Int32})
    beam = KnnResult(index.beam_size)

    for item in reslist[qID]
        push!(beam, item.objID, item.dist)
    end

    for nodeID in index.links[qID]
        if nodeID > qID
            d = convert(Float32, index.dist(index.db[nodeID], index.db[qID]))
            push!(reslist[qID], nodeID, d)
            push!(reslist[nodeID], qID, d)
            push!(beam, nodeID, d)
            push!(tabu, nodeID)
        end
    end
    beam
end

function beam_search_obs_local{T}(index::LocalSearchIndex{T}, qID::Int32, reslist::Vector{KnnResult}, tabu::Set{Int32})
    failures::Int = 0
    improved::Int = 1
    # first beam
    beam::KnnResult = first_beam_local(index, qID, reslist, tabu)
    new_beam::KnnResult = KnnResult(beam.k)
    q = index.db[qID]
    while failures < 1
        length(new_beam) > 0 && clear!(new_beam)
        improved = 0

        for node in beam
            @inbounds for childID in index.links[node.objID]
                if childID > qID && !in(childID, tabu)
                    d = index.dist(index.db[childID], q)
                    push!(tabu, childID)
                    push!(new_beam, childID, d)
                    if push!(reslist[qID], childID, d)
                        improved += 1
                    end
                    # if !in(tabulist[chilID], qID)
                    # push!(tabulist[childID], qID)
                    push!(reslist[childID], qID, d)
                    # end
                end
            end
        end
        
        beam, new_beam = new_beam, beam
        # info("improved=$improved, covrad=$(covrad(res))")
        if improved == 0
            failures += 1
        else
            failures = 0
        end
    end

    beam
end

function compute_aknn{T}(index::LocalSearchIndex{T}, k::Int)
    n = length(index.db)
    reslist = KnnResult[KnnResult(k) for i in 1:n]
    
    for i::Int32 in 1:n
        tabu = Set{Int32}()
        if i % 5000 == 1
            @show i, now()
        end
        push!(tabu, i) # the current object is skipped to avoid early stopping
        beam_search_obs_local(index, i, reslist, tabu)
    end

    return reslist
end
