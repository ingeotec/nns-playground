# installing dependencies

packages = Pkg.installed()
!haskey(packages, "NNS") && Pkg.clone("git@bitbucket.org:ingeotec/nns.jl.git", "NNS")
!haskey(packages, "JSON") && Pkg.add("JSON")
!haskey(packages, "Glob") && Pkg.add("Glob")
!haskey(packages, "DataFrames") && Pkg.add("DataFrames")
Pkg.update()
using Playground

urlpath = "http://ws.ingeotec.mx/~sadit/datasets/"

dblist = [
          "bigann_base-1M.vecs.gz",
          "bigann_query-500.vecs.gz",
          "bigann_base-10M.vecs.gz",
          "bigann_query.vecs.gz",
          #"glove.6B.100d.txt.gz",
          "colors.vecs.gz",
          "colors.vecs.queries.gz",
          "glove.twitter.27B.100d.txt.gz",
          "nasa.vecs.gz",
          "nasa.vecs.queries.gz",
          ]


function create_synthetic_datasets()
    numqueries = 256
    for _n in [1e6]
        n = Int(_n)
        for dim in [2, 4, 8, 16, 32, 64]
            prepare_datasets(n, dim)
            # generate_random_clusters(n, dim, numqueries=numqueries)
        end
    end
end

function fetch_databases()
    mkpath("dbs")
    cd("dbs")
    for name in dblist
        xname = replace(name, ".gz", "")
        if isfile(xname)
            info("$xname was already fetched")
            continue
        end

        cmd = `wget -c $(string(urlpath, name))`
        run(cmd)
        cmd = `gzip -d $(name)`
        run(cmd)
    end
    cd("..")
end

fetch_databases()
