push!(LOAD_PATH, ".")

using NNS
using Playground
using Glob
using JSON
include("obs.jl")


function save_performance(filename, distances, seconds, recall)
    open(filename, "w") do f
        s = json(Dict("distances"=>distances, "seconds"=>seconds, "recall"=>recall), 2)
        write(f, s, "\n")
    end
end

function compute_recall_from_lines(aknn_name, seq, k, test_size)
    lines = open(readlines, aknn_name)
    sample = rand(1:length(seq.db), test_size)
    acc_recall = 0.0
    
    for i in sample
        res1 = eval(parse(lines[i]))
        res2 = search(seq, seq.db[i], KnnResult(k+1))
        shift!(res2); res2.k = k
        r = recall(res1, res2)
        acc_recall += r
    end
    
    return acc_recall / length(sample)
end

function compute_recall(reslist, seq, k, test_size)
    info("computing recall")
    sample = rand(1:length(seq.db), test_size)
    acc_recall = 0.0
    
    for i in sample
        res1 = reslist[i]
        res2 = search(seq, seq.db[i], KnnResult(k+1))
        shift!(res2); res2.k = k
        r = recall(res1, res2)
        acc_recall += r
    end
    
    return acc_recall / length(sample)
end


#const KLIST = [10, 25, 50, 75, 100]
const KLIST = [10, 25, 50]
# const FAILURES = [1, 2, 4, 8]
const BEAM = [1, 2, 4, 8, 16, 32, 64, 128]

# index.algorithm = OBS()  # not necessary if index was created with OBS

# benchmark, loadfun = load_glove("dbs/glove/glove.twitter.27B.100d.txt", 10, 100)
function run_aknn(dbpath, indexname, k, beam_size; test_size=1000)
    benchmark, loadfun = load_vectors(dbpath, k=k, numqueries=test_size)
    perf, seq = loadfun()
    info("loading $(indexname)")
    index = LocalSearchIndex(indexname, seq.db, seq.dist)
    
    aknn_name = string(replace(indexname, "output", "output-aknn"), ".aknn.k=$(k).size=$(test_size)")
    mkpath(dirname(aknn_name))
    #for failures in FAILURES
    # index.max_failures = failures
    
    _aknn_name = string(aknn_name, ".beam_size=$(beam_size)")
    _aknn_name_performance = string(_aknn_name, ".performance")
    if isfile(_aknn_name_performance)
        info("skipping $(_aknn_name_performance)")
        return
    end
        
    info("computing $(_aknn_name_performance)")
    index.beam_size = beam_size
    distances::Float64 = index.dist.calls
    seconds::Float64 = @elapsed reslist = compute_aknn(index, k)
    distances = (index.dist.calls - distances) / length(index.db)
    seconds = seconds / length(index.db)
    recall::Float64 = compute_recall(reslist, seq, k, test_size)
    save_performance(_aknn_name_performance, distances, seconds, recall)
    open(_aknn_name, "w") do f
        for a in reslist
            write(f, string(a), "\n")
        end
    end
end

function print_setup()
    L = []
    for dbpath in [
                   "dbs/nasa.vecs",
                   "dbs/colors.vecs",
                   "dbs/bigann_base-1M.vecs",
                   "dbs/glove.twitter.27B.100d.txt"
                   ]
        dbname = replace(basename(dbpath), ".txt", "")
        indexname = "output/$(dbname)/LocalSearchIndex.search=NNS.BeamSearch().neighborhood=NNS.LogarithmicNeighborhood(2.0f0).recall=0.6"
        for k in KLIST
            for beam_size in BEAM
                # push!(L, x)
                x = (dbpath, indexname, k, beam_size)
                println(string(x))
            end
        end
    end
end

if length(ARGS) == 0
    print_setup()
else
    for inst in ARGS
        t = eval(parse(inst))
        @show inst, t
        run_aknn(t...)
    end
end

