# Near Neighbor Search (NNS) Playground #


## What is this repository for? ##
* This repository contains a list of modules and scripts to test and experiment on the state of the art algorithms for near-neighbor searching.
* Version: This code is alpha-version

## How do I get set up? ##

* Get [julia](http://julialang.org/downloads/)  version 0.4.5. Install it and set the executable into your PATH.

* Install the necessary packages and retrieve some datasets. Both things are performed using `julia prepare-workdir.jl`. Firstly, check and edit `prepare-workdir.jl` to fit your needs.

## Experiment about approximate metric indexes ##
* Create some indexes with `julia gen-approx.jl | julia run-index.jl`.
* If you have a multi core computer, you should use `julia gen-approx.jl | python dist.py run-index.jl` to take advantage of the multiprocessing capabilities of your computer. Please review `dist.py` to see the details.
* NOTE: Please edit `gen-approx.jl` to fit your needs.

## Computing AKNN ##
You need some indexes of the type LocalSearchIndex, in particular, `run-aknn.jl` uses `BeamSearch` with `LogarithmicNeighborhood` (check the `NNS.jl` module to get the details.
Running `julia run-aknn.jl` without arguments will generate a list of instructions. In order to execute them, you must run `julia run-aknn.jl | python dist.py run-aknn.jl`

### Contribution guidelines ###

* Write new indexes
* Applications
* Writing tests
* Code review

### Who do I talk to? ###

Please send me an e-mail or use the integrated tools for collaboration (?)